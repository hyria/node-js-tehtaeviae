const readline = require('node:readline');
const { stdin: input, stdout: output } = require('node:process');

const rl = readline.createInterface({ input, output });
let aurinkoa = false;
rl.question('paistaako aurinko? (k/e)', function(answer) {
    if(answer === 'k') {
        aurinkoa = true, 
        console.log("aurinko paistaa");
    }
    else
        console.log("aurinko ei paista")

let kylmä = false;
rl.question('onko talvi? (k/e)', function(wer) {
    if(wer === 'k') {
        kylmä = true, 
        console.log("kylmää");
    }
    else
        console.log("ei ole talvi")
        
    rl.close();
});
});

function palaute(aurinkoa, kylmä) {
    if(aurinkoa && kylmä) {
        console.log("talvi on tulossa");
    } else if(!aurinkoa && kylmä) {
        console.log("on talvi")
    } else if(aurinkoa && !kylmä) {
        console.log("trooppinen keli")
    }
}